# JavaScript-Unit-Testing-with-Mocha-Chai-and-Sinon
[TutsPlus] JavaScript Unit Testing with Mocha, Chai and Sinon with Jason Rhodes [ENG, 2014]

___

src: https://github.com/tutsplus/javascript-unit-testing-mocha-chai-sinon
___


## 01 - Introduction

### 1.2. Getting Started  

    npm install --save express
    npm install --save jade

    nodemon server.js


### 1.3. Writing Your First Test

    npm install --save-dev mocha@1.18.2
    mocha
    mocha --reporter spec


### 1.4. Building a Test Suite

    mocha -R spec


### 1.5. Mocha on the Command Line

    mocha -h
    mocha -g hex
    mocha -R list -g hex
    mocha -b
    mocha -w


## 02 - Fundamental Concepts of JavaScript Unit Testing

### 2.1. Dependency Injection  

    npm test

### 2.2. Asynchronous Testing  

    npm test


### 2.3. Before and After Hooks

    npm test

### 2.4. Segmenting with Skip and Only


### 2.5. Fixtures

    npm install --save-dev chai@1.9.1
    npm test


## 03 - Using Chai Assertions  (assert -> should --> expect)

### 3.1. Chai Assert

    npm test

### 3.2. Chai Should

    npm test


### 3.3. Chai Expect

npm test


## 04 - Putting it All Together


### 4.1. Running Mocha Tests in the Browser

    mocha init browser
    npm install -g browserify
    browserify test/hex2rgb.test.js -o browser/test.js
    open browser/index.html


### 4.2. Automated Browser Testing with Testling  

  https://ci.testling.com/  

    npm install -g testling
    testling

https://ci.testling.com/guide/quick_start

    testling
    testling -u


### 4.3. Continuous Integration with Travis CI


## 05 - More Control with Sinon.JS  

### 5.1. Introduction to Sinon

http://sinonjs.org/  

    npm install --save-dev sinon
    npm test


### 5.2. Sinon Spies

    npm test

### 5.3. Sinon Stubs

    npm test

### 5.4. Sinon Mocks

    npm test
